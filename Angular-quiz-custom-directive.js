//Custom button directive creation takes place here. It makes it so easy............
quizApp.directive('buttonCreator', function() {
    return {
        restrict: 'E',
        scope: {
            buttonName: '@',
            buttonContent: '=',
            buttonSelected: '&',
            customClassName: '@'
        },
       // templateUrl:'Custom-Button-Template.html'
        template: '<button class = "{{customClassName}}" ng-click = "buttonSelected()">'+
                  '{{ buttonName }}'+'</button>'
    };
});