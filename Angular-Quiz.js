
//use web service to fetch question data.
quizApp.controller('angularQuizCntrl', ['$scope','$http',function($scope,$http) {
    $http.get("Questions.json").success(function(response) {
      $scope.questionBank = response;
      //initialize with first question and option because it is loaded by default when user see the page. 
      $scope.currentQuestion = $scope.questionBank[0].text;
      $scope.currentOptions = $scope.questionBank[0].options;
      $scope.answeredOptions = [];
      $scope.answeredQuestionWiseMark = [];
      $scope.answerBooklet = [];
      $scope.questionNum = 1;
      $scope.index = 0;
      $scope.complete = false; 
      $scope.totalMark = 0;
      //Store correct answers into an array which is used to display when Quiz end.
      for(var i = 0 ;i < $scope.questionBank.length ; i++){
        $scope.answerBooklet.push($scope.questionBank[i].answer);
      }
      //function included in custom directive to trigger actions when user select an option.  
    });
     $scope.answered = function()  {
        $scope.answeredOptions.push(event.target.innerHTML);
        if(event.target.innerHTML == $scope.questionBank[$scope.index].answer){
              console.log($scope.questionBank[$scope.index].answer);
              $scope.totalMark++;
              $scope.answeredQuestionWiseMark.push(1);
          }else{
              $scope.answeredQuestionWiseMark.push(0);
          }
        if($scope.questionNum < $scope.questionBank.length) {
          $scope.currentQuestion = $scope.questionBank[$scope.questionNum].text;
          $scope.currentOptions = $scope.questionBank[$scope.questionNum].options;
          $scope.questionNum++;
          $scope.index++;
         } else {
              $scope.complete = true;
           }
    }
    //function invoked when user pressed try again button.
    $scope.tryAgain = function(){
      $scope.currentQuestion = $scope.questionBank[0].text;
      $scope.currentOptions = $scope.questionBank[0].options;
      $scope.answeredOptions = [];
      $scope.answeredQuestionWiseMark = [];
      $scope.questionNum = 1;
      $scope.index = 0;
      $scope.complete = false; 
      $scope.totalMark = 0;
    }
}]);
//Custom button directive creation takes place here. It makes it so easy............
quizApp.directive('buttonCreator', function() {
    return {
        restrict: 'E',
        scope: {
            buttonName: '@',
            buttonContent: '=',
            buttonSelected: '&',
            customClassName: '@'
        },
        template: '<button class = "{{customClassName}}" ng-click = "buttonSelected()">'+
                  '{{ buttonName }}'+'</button>'
    };
});