//$http is injected to fetch json data information.
quizApp.controller('angularQuizCntrl', ['$scope','$http',function($scope,$http) {
    $http.get("Questions.json").success(function(response) {
      $scope.questionBank = response;
      //initialize with first question and option because it is loaded by default when user see the page. 
      $scope.currentQuestion = $scope.questionBank[0].text;
      $scope.currentOptions = $scope.questionBank[0].options;
      $scope.answeredOptions = [];
      $scope.answeredQuestionWiseMark = [];
      $scope.answerBooklet = [];
      $scope.questionNum = 1;
      $scope.index = 0;
      $scope.complete = false; 
      $scope.totalMark = 0;
      //Store correct answers into an array which is used to display when Quiz end.
      for(var i = 0 ;i < $scope.questionBank.length ; i++) {
        $scope.answerBooklet.push($scope.questionBank[i].answer);
      } 
    });
     //functions fired when used as attribute in  custom directive. 
    $scope.answered = function()  {
      $scope.answeredOptions.push(event.target.innerHTML);
      if(event.target.innerHTML == $scope.questionBank[$scope.index].answer) {
        $scope.totalMark++;
        $scope.answeredQuestionWiseMark.push(1);
        } else {
            $scope.answeredQuestionWiseMark.push(0);
          }
      if($scope.questionNum < $scope.questionBank.length) {
        $scope.currentQuestion = $scope.questionBank[$scope.questionNum].text;
        $scope.currentOptions = $scope.questionBank[$scope.questionNum].options;
        $scope.questionNum++;
        $scope.index++;
       } else {
            $scope.complete = true;
         }
    }
    //function invoked when user pressed try again button.
    $scope.tryAgain = function() {
      $scope.currentQuestion = $scope.questionBank[0].text;
      $scope.currentOptions = $scope.questionBank[0].options;
      $scope.answeredOptions = [];
      $scope.answeredQuestionWiseMark = [];
      $scope.questionNum = 1;
      $scope.index = 0;
      $scope.complete = false; 
      $scope.totalMark = 0;
    }
}]);
